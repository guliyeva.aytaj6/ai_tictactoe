"""
Tic Tac Toe Player
"""

import copy
import math
from os import PathLike

X = "X"
O = "O"
EMPTY = None


def initial_state():
    """
    Returns starting state of the board.
    """
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
   Returns player who has the next turn on a board.
   """

    x_count, o_count = 0, 0
    for row in board:
        for item in row:
            if item == X:
                x_count += 1
            elif item == O:
                o_count += 1
    return O if x_count > o_count else X


def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    tuples = []
    for i in range(len(board)):
        for j in range(3):
            if board[i][j] == EMPTY:
                tuples.append((i, j))

    return tuples


class InvalidAction(Exception):
    """This action is not possible"""
    pass


def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    if action not in actions(board):
        raise InvalidAction
    copyboard = copy.deepcopy(board)
    if copyboard[action[0]][action[1]] != EMPTY:
        raise InvalidAction
    copyboard[action[0]][action[1]] = player(board)
    return copyboard


def winner(board):
    """
  Returns the winner of the game, if there is one.
  """

    # win by column
    for i in range(len(board)):
        if board[i][0] == board[i][1] == board[i][2]:
            if board[i][0] == X:
                return X
            elif board[i][0] == O:
                return O
            else:
                return None

    # win by row
    for i in range(3):
        if board[0][i] == board[1][i] == board[2][i]:
            if board[0][i] == X:
                return X
            elif board[0][i] == O:
                return O
            else:
                return None

    # win by diagonal
    if board[0][0] == board[1][1] == board[2][2]:
        if board[0][0] == X:
            return X
        elif board[0][0] == O:
            return O
    elif board[0][2] == board[1][1] == board[2][0]:
        if board[0][2] == X:
            return X
        elif board[0][2] == O:
            return O
        else:
            return None


def terminal(board):
    """
    Returns True if game is over, False otherwise.
    """

    if winner(board) == X or winner(board) == O or len(actions(board)) == 0:
        return True
    else:
        return False


def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """

    if winner(board) == X:
        return 1
    elif winner(board) == O:
        return -1
    else:
        return 0


def min_value(board):
    if terminal(board):
        return utility(board)
    k = math.inf
    for action in actions(board):
        value = max_value(result(board, action))
        if value < k:
            k = value
    return k


def max_value(board):
    if terminal(board):
        return utility(board)
    k = -math.inf
    for action in actions(board):
        value = min_value(result(board, action))
        if value > k:
            k = value
    return k


def minimax(board):
    """
    Returns the optimal action for the current player on the board.
    """
    opt_act = None

    if terminal(board):
        return None

    if player(board) == X:
        inf = -math.inf
        for action in actions(board):
            value = min_value(result(board, action))
            if value > inf:
                inf = value
                opt_act = action

    elif player(board) == O:
        inf = math.inf
        for action in actions(board):
            value = max_value(result(board, action))
            if value < inf:
                inf = value
                opt_act = action
    return opt_act
